#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pds_file_t.h"

#define PDS_MAX_FDS 8

struct pds_file_s {
	long pds_bpos;
	long pds_fpos;
	long pds_nchar;
	int pds_fmodif;
	int pds_fd;
	int pds_busy;
	int pds_buf_size;
	char *pds_buf;
};

static pds_file_t pds_fds[PDS_MAX_FDS];

pds_file_t * pds_stdin  = pds_fds;
pds_file_t * pds_stdout = pds_fds + 1;
pds_file_t * pds_stderr = pds_fds + 2;

pds_file_t * pds_fopen(const char * path) {
	/* Search for an empty place */
	pds_file_t * res;
	int i;
	for (i = 0; i < PDS_MAX_FDS; i++) {
		if (!pds_fds[i].pds_busy) {
			res = &pds_fds[i];
			break;
		}
	}

	if (i == PDS_MAX_FDS) {
		fprintf(stderr, "Reached max value of file descriptors\n");
		exit(EXIT_FAILURE);
	}
	
	/* Initialize content of struct */
	res->pds_busy = 1;
	res->pds_bpos = res->pds_fpos = res->pds_nchar = res->pds_fmodif = 0;
	res->pds_buf_size = 4096;
	
	if ((res->pds_fd = open(path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IWOTH | S_IROTH)) == -1) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	res->pds_buf = malloc(res->pds_buf_size + sizeof(char));

	if (res->pds_buf == NULL) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	
	if ((res->pds_nchar = read(res->pds_fd, res->pds_buf, res->pds_buf_size)) == -1) {
		perror("read");
		exit(EXIT_FAILURE);
	}

	if (res->pds_nchar < res->pds_buf_size) {
		memset(res->pds_buf + res->pds_nchar, 0, res->pds_buf_size - res->pds_nchar);
	}
	
	return res;
}

int pds_fflush(pds_file_t * stream) {
	if (write(stream->pds_fd, stream->pds_buf, stream->pds_buf_size) == -1) {
		perror("write");
		exit(EXIT_FAILURE);
	}

	return 0;
}

int pds_fclose(pds_file_t * stream) {
	if (stream->pds_busy) {
		stream->pds_busy = 0;
		free(stream->pds_buf);
	}

	return 0;
}
