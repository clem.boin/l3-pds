set title "Taille du buffer et Vitesse d'execution"
set logscale x
set xlabel "Taille du buffer"
set logscale y
set ylabel "Temps en secondes"
set style data linespoints
plot "mcat-tm.dat" using 1:2 title "taille", \
     "mcat-tm.dat" using 1:3 title "vitesse"
pause -1  "Hit return to continue"