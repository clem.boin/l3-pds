TP Perf
=======

Ce dossier contient les fichiers visant à analyser la vitesse d'execution
d'un programme en fonction de la taille d'un buffer.

Contenu
-------

- README.md
  Ce fichier, expliquant comment fonctionne l'ensemble des programmes

- Makefile
  Fichier donnant les regles de compilation

- mcat-scd.c
  Code d'une version très simplifiée de cat

- run.gnu
  Fichiers definissant les paramètres pour gnuplot

- mcat.sh
  Script shell generant un fichier mcat-tm.dat contenant le temps l'execution
  de l'executable mcat-scd generée par rapport à la taille du buffer MCAT_BUFSIZ

- graph.pdf
  Resultat graphique du resultat mcat-tm.dat generé par le script mcat.sh

Utilisation
-----------

Pour generer les fichiers binaires :
```sh
$ make
```

Pour tester l'executable généré :
```sh
$ ./mcat-scd /path/to/file
```

Pour generer un fichier mcat-tm.dat :
```sh
$ make dat
```

Pour generer le graphique avec gnuplot :
```sh
$ gnuplot run.gnu
```

Auteur
------

Nicolas SEYS - L3 Groupe 3