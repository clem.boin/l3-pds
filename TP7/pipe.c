/* mshell - a job manager */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "pipe.h"
#include <sys/types.h>
#include <sys/wait.h>

void do_pipe(char *cmds[MAXCMDS][MAXARGS], int nbcmd, int bg) {
	int filedes[MAXCMDS][2];
	size_t i, j;
	
	if (verbose) {
		printf("do_pipe : entering\n");
	}
	
	/* Creating the pipes using the fd tab */
	for (i = 0; i < nbcmd; i++) {
		if (pipe(filedes[i]) == -1) {
			unix_error("do_pipe : error pipe");
		}
	}

	/* Treating the first command */
	switch (fork()) {
	case -1:
		unix_error("do_pipe : error first fork");
	case 0:
		/* Duplicating the pipe output on the standard output */
		dup2(filedes[0][1], STDOUT_FILENO)

		/* Closing the unused pipes */
		close(filedes[0][0]);

		/* Executing the first command */
		execvp(cmds[0][0], cmds[0]);
		unix_error("do_pipe : error first execvp");
	default:
		break;
	}

	/* Treating from the second command to the n-1th */
	for (i = 1; i < nbcmd - 1; i++) {
		switch (fork()) {
		case -1:
			unix_error("do_pipe : error middle fork");
		case 0:
			/* Duplicating the pipe input on the standard input */
			dup2(filedes[i - 1][0], STDIN_FILENO)
			/* Duplicating the pipe output on the standard output */
			dup2(filedes[i][1], STDOUT_FILENO)

			/* Closing the unused pipes */
			for (j = 0; j < i; j++) {
				if (j != i - 1) {
					close(filedes[j][0]);
				}
				if (j != i) {
					close(filedes[j][1]);
				}
			}

			/* Executing the ith command */
			execvp(cmds[i][0], cmds[i]);
			unix_error("do_pipe : error middle execvp");
		default:
			break;
		}
	}

	/* Treating the last command */
	switch (fork()) {
	case -1:
		unix_error("do_pipe : error last fork");

	case 0:
		/* Duplicating the pipe input on the standard input */
		dup2(filedes[nbcmd - 2][0], STDIN_FILENO)
		
		/* Closing the unused pipes */
		close(filedes[nbcmd - 2][1]);
		for (i = 0; i < nbcmd - 1; i++) {
			close(filedes[i][0]);
			close(filedes[i][1]);
		}

		/* Executing the last command */
		execvp(cmds[nbcmd - 1][0], cmds[nbcmd - 1]);
		unix_error("do_pipe : error last execvp");
	default:
		break;
	}

	/* Closing the unused pipes */
	for (i = 0; i < nbcmd; i++) {
		close(filedes[i][0]);
		close(filedes[i][1]);
	}

	/* Waiting for the process */
	for (i = 0; i < nbcmd; i++) {
		if (bg) {
			waitpid(-1, NULL, WNOHANG);
		} else {
			wait(NULL);
		}
	}

	if (verbose) {
		printf("do_pipe : exiting\n");
	}
}
