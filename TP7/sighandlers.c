/* mshell - a job manager */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>

#include "jobs.h"
#include "common.h"
#include "sighandlers.h"

/*
 * wrapper for the sigaction function
 */
int sigaction_wrapper(int signum, handler_t * handler) {
	struct sigaction action;

	if (verbose)
		printf("[verbose] sigaction_wrapper: entering\n");

	action.sa_handler = handler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = SA_RESTART;

	if (sigaction(signum, &action, NULL) < 0)
		unix_error("sigaction_wrapper error");

	if (verbose)
		printf("[verbose] sigaction_wrapper: ending\n");

	return 1;
}

/*
 * sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
 *     a child job terminates (becomes a zombie), or stops because it
 *     received a SIGSTOP or SIGTSTP signal. The handler reaps all
 *     available zombie children
 */
void sigchld_handler(int sig) {
	struct job_t *j;
	pid_t child_pid;
	int status;

	if (verbose)
		printf("[verbose] sigchld_handler: entering\n");

	while ((child_pid = waitpid(-1, &status, WNOHANG | WUNTRACED)) > 0) {
		if (WIFSTOPPED(status)) {
			j->jb_state = ST;
			fprintf(stdout, "Job [%d] (%d) stopped by signal %d\n", j->jb_jid, child_pid, WSTOPSIG(status));
		} else if (WIFSIGNALED(status) || WIFEXITED(status)) {
			j = jobs_getjobpid(child_pid);

			if (j == NULL) {
				unix_error("jobs_getjobpid error");
			}
					
			jobs_deletejob(child_pid);
		} else {
			unix_error("waitpid error");
		}
	}

	if (verbose)
		printf("[verbose] sigchld_handler: exiting\n");
}

/*
 * sigint_handler - The kernel sends a SIGINT to the shell whenver the
 *    user types ctrl-c at the keyboard.  Catch it and send it along
 *    to the foreground job.
 */
void sigint_handler(int sig) {
	pid_t pid;

	if (verbose)
		printf("[verbose] sigint_handler: entering\n");

	if ((pid = jobs_fgpid()) > 0) {
		if (kill(pid, SIGINT) < 0) {
			unix_error("sigint_handler error");
		}
	}

	if (verbose)
		printf("[verbose] sigint_handler: exiting\n");
}

/*
 * sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
 *     the user types ctrl-z at the keyboard. Catch it and suspend the
 *     foreground job by sending it a SIGTSTP.
 */
void sigtstp_handler(int sig) {
	pid_t pid;

	if (verbose)
		printf("[verbose] sigtstp_handler: entering\n");

	if ((pid = jobs_fgpid()) > 0) {
		if (kill(pid, SIGTSTP) < 0) {
			unix_error("sigstp_handler error");
		}
	}

	if (verbose)
		printf("[verbose] Sigtstp_Handler: exiting\n");
}
