#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>

#define NB_THREADS 4

pthread_t thread_ids[NB_THREADS];

struct pt_compteur_s {
	char *bloc;
	unsigned long taille;
};

unsigned long compteur_gc(char *bloc, unsigned long taille) {
    static unsigned long cptr = 0;
	unsigned long i, tmp;
	pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

	pthread_mutex_lock(&mutex);

    for (i = 0; i < taille; i++) {
        if (bloc[i] == 'G' || bloc[i] == 'C') {
			printf("%s %lu %lu\n", bloc, cptr, taille);
            cptr++;
			printf("%s %lu %lu\n", bloc, cptr, taille);
		}
	}
	tmp = cptr;

	pthread_mutex_unlock(&mutex);
	
    return tmp;
}

void *pt_compteur_gc(void *arg) {
	struct pt_compteur_s *args = (struct pt_compteur_s *) arg;
	return (void *) compteur_gc(args->bloc, args->taille);
}

int main(int argc, char *argv[]) {
    struct stat st;
    int fd, lus, i;
    char *tampon;
    unsigned long cptr = 0;
	void *value;
    off_t taille = 0;
    struct timespec debut, fin;
	pthread_t threadid;
	struct pt_compteur_s * s_comp;
	
    assert(argv[1] != NULL);

    /* Quelle taille ? */
    assert(stat(argv[1], &st) != -1);
    tampon = malloc(st.st_size);
    assert(tampon != NULL);

    /* Chargement en mémoire */
    fd = open(argv[1], O_RDONLY);
    assert(fd != -1);
    while ((lus = read(fd, tampon + taille, st.st_size - taille)) > 0)
        taille += lus;
    assert(lus != -1);
    assert(taille == st.st_size);
    close(fd);

/* struct pt_compteur_s { */
/* 	int num; */
/* 	char *bloc; */
/* 	unsigned long taille; */
/* }; */

	assert(clock_gettime(CLOCK_MONOTONIC, &debut) != -1);
	
	/* Création des threads */
    for (i = 1; i < NB_THREADS; i++) {
		s_comp = malloc(sizeof(struct pt_compteur_s));
		s_comp->taille = taille / NB_THREADS;
		s_comp->bloc = tampon + (i - 1) * s_comp->taille;
        pthread_create(&threadid, NULL, &pt_compteur_gc, s_comp);
    }
	
    /* Calcul proprement dit */
    /* cptr = compteur_gc(tampon, taille); */
	pthread_join(threadid, &value);
	cptr = (long) value;
    assert(clock_gettime(CLOCK_MONOTONIC, &fin) != -1);
	free(s_comp);
	
    /* Affichage des résultats */
    printf("Nombres de GC:   %ld\n", cptr);
    printf("Taux de GC:      %lf\n", ((double) cptr) / ((double) taille));

    fin.tv_sec  -= debut.tv_sec;
    fin.tv_nsec -= debut.tv_nsec;
    if (fin.tv_nsec < 0) {
        fin.tv_sec--;
        fin.tv_nsec += 1000000000;
    }
    printf("Durée de calcul: %ld.%09ld\n", fin.tv_sec, fin.tv_nsec);
    printf("(Attention: très peu de chiffres après la virgule sont réellement significatifs !)\n");

    return 0;
}
