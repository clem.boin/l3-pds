#include <pthread.h>
#include <stdlib.h>

#define SIZE 8

struct pt_search_s {
	int *tab;
	unsigned int size;
	int (*pred)(int);
};

void *pt_search(void *arg);
int search(int *tab, unsigned int size, int (*pred)(int));

int search(int *tab, unsigned int size, int (*pred)(int)) {
	pthread_t tid;
	void *ret;
	int i;

	if (size < SIZE) {
		for (i = 0; i < size; i++) {
			if (pred(tab[i])) {
				return i;
			}
		}
		return -1;
	}

	/* creation d'un thread pour size/2 taille */
	struct pt_search_s args;
	args.tab = tab;
	args.size = size / 2;
	args.pred = pred;
	pthread_create(&tid, NULL, &pt_search, (void *)&args);

	/* recherche dans la seconde moitié du tableau */
	i = search(tab + size / 2, size / 2, pred);
	if (i != -1) i += size / 2;

	/* recuperer le retour de thread et combiner les resultats */
	pthread_join(tid, &ret);
	if ((int) ret == -1)
		return i;
	else
		return (int) ret;
}

void *pt_search(void *arg) {
	struct pt_search_s *args = (struct pt_search_s *) arg;
	return (void *) search(args->tab, args->size, args->pred);
}

int main(void) {

	return EXIT_SUCCESS;
}
