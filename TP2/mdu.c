#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <libgen.h>
#include <search.h>

#define MAX_KEY 1024

static int opt_follow_links = 0;
static int opt_apparent_size = 0;

void usage() {
	printf("Usage:\n");
	printf("  mdu [options] <path>\n");
	printf("\n");
	printf("Options:\n");
	printf("  -b\tPrint apparent sizes, rather than disk usage.\n");
	printf("  -L\tDereference all symbolic links.\n");
	exit(EXIT_FAILURE);
}

int valid_name(const char *name) {
	return strcmp(name, ".") & strcmp(name, "..");
}

int du_file(const char *pathname) {
	ENTRY e;
	int ret_value, size = 0;
	char key[MAX_KEY];
	struct stat st;

	if (opt_follow_links)
		ret_value = stat(pathname, &st);
	else
		ret_value = lstat(pathname, &st);

	/* Test if stat or lstat failed */
	if (ret_value != 0) {
		if (opt_follow_links)
			perror("stat");
		else
			perror("lstat");
		return EXIT_FAILURE;
	}

	/* Setting up the Entry */
	snprintf(key, MAX_KEY, "%ld%ld", st.st_ino, st.st_dev);
	e.key  = key;
	e.data = &pathname;

	/* If the Entry is not found, we can add the size */
	if (!hsearch(e, FIND)) {
		hsearch(e, ENTER);
		size += opt_apparent_size ? st.st_size : st.st_blocks;
	}

	/* Regular File or Symbolic Link */
	if (S_ISREG(st.st_mode) || S_ISLNK(st.st_mode)) {
		return size;
	}
	
	/* Directory */
	if (S_ISDIR(st.st_mode)) {
		char path_entry[PATH_MAX - 1];
		struct dirent *stdirent;
		DIR *dir;

		dir = opendir(pathname);

		while ((stdirent = readdir(dir)) != NULL) {
			if (!valid_name(stdirent->d_name))
				continue;

			snprintf(path_entry, PATH_MAX, "%s/%s",
					 pathname, stdirent->d_name);
			size += du_file(path_entry);
		}

		closedir(dir);

		printf("%d\t%s\n", size, pathname);
		return size;
	}

	fprintf(stderr, "Type de fichier non pris en compte %s\n", pathname);
	return 0;
}

int main(int argc, char **argv) {
	int opt;
	char *pathname = NULL;

	if (hcreate(256) == 0) {
		perror("hcreate");
		return EXIT_FAILURE;
	}
	
	while ((opt = getopt(argc, argv, "bL")) != -1) {
		switch (opt) {
		case 'b':
			opt_apparent_size = 1;
			break;
		case 'L':
			opt_follow_links = 1;
			break;
		case '?':
		default:
			usage();
		}
	}

	pathname = argv[optind];

	if (pathname == NULL) {
		printf("%s: Missing argument\n", basename(argv[0]));
		usage();
	}

	du_file(argv[optind]);
	hdestroy();
	
	return EXIT_SUCCESS;
}
