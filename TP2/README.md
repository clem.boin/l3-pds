TP PDS 2 - mdu
==============

Ce dossier contient les fichiers des exercices "Parcours d’une hiérarchie" du deuxième TP de PDS.

Contenu
-------

- Readme.md
  Ce fichier, expliquant comment fonctionne l'essemble des programmes

- Makefile
  Fichier donnant les regles de compilation

- mdu.c
  Code d'une version simplifiée de du

Executer
--------

Pour generer les fichiers binaires, executer :
```sh
$ make
```

Puis executer le fichier mdu généré.
Pour afficher le mode d'utilisation de mdu, le lancer simplement sans options :
```sh
$ ./mdu
```

Pour effectuer les tests, lancer :
```sh
$ make test
```

Ou :
```sh
$ ./tests.sh
```

Auteur
------

Nicolas SEYS - L3 Groupe 3