#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

static void f(int sec, const char *fname) {
	sleep(sec);
	fprintf(stderr, "Function %s() executed by process %d\n",
			fname, getpid());
}

static void fa() { f(4, "fa"); }
static void fb() { f(2, "fb"); }
static void fc() { f(3, "fc"); }

void trif(void(*f1)(void), void(*f2)(void), void(*f3)(void)) {
	switch(fork()) {
	case -1:
		perror("fork");
		exit(-1);
	case 0:
		f1();
		exit(0);
	default:
		break;
	}

	switch(fork()) {
	case -1:
		perror("fork");
		exit(-1);
	case 0:
		f2();
		exit(0);
	default:
		break;
	}

	switch(fork()) {
	case -1:
		perror("fork");
		exit(-1);
	case 0:
		f3();
		exit(0);
	default:
		break;
	}

	wait(NULL);wait(NULL);wait(NULL);
}

int main(void) {
	trif(fa, fb, fc);
	fprintf(stderr, "term main()\n");

	return 0;
}
