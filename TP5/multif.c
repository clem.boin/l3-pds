#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

typedef int (*func_t) (int);

static void f(int sec, const char *fname) {
	printf("JE SUIS %s ET JE DORS %d S\n", fname, sec);
	sleep(sec);
	printf("JE SUIS %s ET J'AI BIEN DORMI !\n", fname);
}

static void fa(int i) { f(i, "fa"); }
static void fb(int i) { f(i, "fb"); }
static void fc(int i) { f(i, "fc"); }

int multif (func_t f[], int args[], int n) {
	int i, res = 0, status;
	
	for (i = 0; i < n; i++) {
		switch (fork()) {
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		case 0:
			f[i](args[i]);
			exit(EXIT_SUCCESS);
		default:
			break;
		}
	}

	for (i = 0; i < n; i++) {
		wait(&status);
		if (WIFEXITED(status))
			res += WEXITSTATUS(status);
	}
	
	return res;
}

int main(void) {
	func_t f[3] = {(func_t)fa, (func_t)fb, (func_t)fc};
	int args[3] = {3, 2, 4};

	printf("MULTIF M'A RETOURNÉ : %d !\n", multif(f, args, 3));
	return EXIT_SUCCESS;
}
