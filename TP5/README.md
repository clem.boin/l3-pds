TP PDS 5 - Zombie & Course
==========================

Ce dossier contient les fichiers du 5ème TP de PDS.

Contenu
-------

- README.md
  Ce fichier, expliquant comment fonctionne l'ensemble des programmes

- Makefile
  Fichier donnant les regles de compilation

- multif.c
  Code de l'exercice "Multi-fourche"

- zombie.c
  Code de l'exercice "Pas de zombis"

- course.c
  Code de l'exercice "À vos marques, prêts..."

Executer
--------

Pour generer les fichiers binaires, executer :
```sh
$ make
```

Puis executer le fichier executable voulu.

Réponses aux questions
----------------------

- Question 2 : Comment faire en sorte que le père récupère le PID de son petit-fils ?
  Une solution aurait été de recuperer la valeur du petit-fils en retournant le pid
  de ce dernier grâce à un exit(pid) sur le fils, mais cela n'est pas possible, car
  lorsque l'on souhaite recuperer la valeur de retour grâce à la variable status,
  la partie qui code le code de retour est sur 8 bits, et les numéros de pid sont
  très souvent supérieurs à 255.

- Question 3 : Que penser, en terme de coût de copie mémoire, de cette technique du double fork ?
  Étant donné que l'execution d'un fork, copie beaucoup d'elements initialement présents
  dans l'execution du père, effectuer deux fois un fork réalise deux fois cette opération,
  et est donc couteux.

Auteur
------

Nicolas SEYS - L3 Groupe 3