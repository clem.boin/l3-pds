#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void) {
	int i, j;
	pid_t pid;
	
	for (i = 0; i < 10; i++) {
		switch (fork()) {
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		case 0:
			printf("FILS %d A DEMARRÉ\n", getpid());
			for (j = 0; j < 1000000; j++);
			printf("FILS %d PREMIER PASSAGE\n", getpid());
			for (j = 0; j < 1000000; j++);
			printf("FILS %d DEUXIÈME PASSAGE\n", getpid());
			exit(EXIT_SUCCESS);
		default:
			break;
		}
	}

	while ((pid = wait(NULL)) != -1) {
		printf("LE FILS %d EST ARRIVÉ\n", pid);
	}
	
	return EXIT_SUCCESS;
}
