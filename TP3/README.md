TP PDS 3 - mtail
================

Ce dossier contient les fichiers des exercices "mtail" du troisième TP de PDS.

Contenu
-------

- README.md
  Ce fichier, expliquant comment fonctionne l'ensemble des programmes

- Makefile
  Fichier donnant les regles de compilation

- mtail.c
  Code d'une version simplifiée de tail

Executer
--------

Pour generer les fichiers binaires, executer :
```sh
$ make
```

Puis executer le fichier mtail généré.
Pour afficher le mode d'utilisation de mtail, le lancer simplement sans options :
```sh
$ ./mtail
```

Pour effectuer les tests, lancer :
```sh
$ make test
```

Ou :
```sh
$ ./tests.sh
```

Notes
-----
La version efficace de tail ne fonctionne pas correctement lorsque le nombre de lignes voulues
est trop grand (depend selon la taille des lignes)

Auteur
------

Nicolas SEYS - L3 Groupe 3