#!/bin/bash

if [ ! -f ./do ]
then
	echo "Executez 'make' avant d'effectuer les tests"
	exit -1
fi

# Test 1 : Execute une seule commande qui réussit
echo "Test 1 : Execute une seule commande qui réussit"
./do true && echo "Test 1 - OK" || echo "Test 1 - Erreur: true à renvoyé false"
echo

# Test 2 : Execute une seule commande qui échoue
echo "Test 2 : Execute une seule commande qui échoue"
./do false && echo "Test 2 - Erreur: false à renvoyé true" || echo "Test 2 - OK" 
echo

# Test 3 : Mode "and" et au moins une des commandes échoue
echo "Test 3 : Mode 'and' et au moins une des commandes échoue"
./do --and true false && echo "Test 3 - Erreur: and true false a renvoyé true" || echo "Test 3 - OK"
echo

# Test 4 : Mode "and" et toutes les commandes réussissent
echo "Test 4 : Mode 'and' et toutes les commandes réussissent"
./do --and true true && echo "Test 4 - OK" || echo "Test 4 - Erreur: and true true a renvoyé false"
echo

# Test 5 : Mode "and" et toutes les commandes échouent
echo "Test 5 : Mode 'and' et toutes les commandes échouent"
./do --and false false && echo "Test 5 - Erreur: or false false a renvoyé true" || echo "Test 5 - OK"
echo

# Test 6 : Mode 'or' et au moins une des commandes échoue
echo "Test 6 : Mode 'or' et au moins une des commandes échoue"
./do --or true false && echo "Test 6 - OK" || echo "Test 6 - Erreur: or true false à renvoyé false"
echo

# Test 7 : Mode 'or' et toutes les commandes réussissent
echo "Test 7 : Mode 'or' et toutes les commandes réussissent"
./do --or true true && echo "Test 7 - OK" || echo "Test 7 - Erreur : or true true à renvoyé false"
echo

# Test 8 : Mode 'or' et toutes les commandes échouent
echo "Test 8 : Mode 'or' et toutes les commandes échouent"
./do --or false false && echo "Test 8 - Erreur: or false false à renvoyé true" || echo "Test 8 - OK"
echo

# Test 9 : Mode 'and + cc' et une des commandes échoue plus vite que la terminaison des autres
echo "Test 9 : Mode 'and + cc' et une des commandes échoue plus vite que la terminaison des autres"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc "sleep 2" false && echo "Test 9 - Erreur: and cc 'sleep 1' false a renvoyé true" || echo "Test 9 - OK"
[[ ! nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 9 (bis) - OK" || echo "Test 9 (bis) - Erreur: sleep pas executé"
echo

# Test 10 : Mode 'and + cc' et les commandes n’échouent pas
echo "Test 10 : Mode 'and + cc' et les commandes n’échouent pas"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc "sleep 2" true && echo "Test 10 - OK" || echo "Test 10 - Erreur: and cc 'sleep 1' false a renvoyé true" 
[[ ! nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 10 (bis) - OK" || echo "Test 10 (bis) - Erreur: sleep pas executé"
echo

# Test 11 : Mode 'or + cc' et une des commandes échoue plus vite que la terminaison des autres
echo "Test 11 : Mode 'or + cc' et une des commandes échoue plus vite que la terminaison des autres"
nbsleep=$(pgrep sleep | wc -l)
./do --or --cc "sleep 2" false && echo "Test 11 - Erreur: and cc 'sleep 1' false a renvoyé true" || echo "Test 11 - OK"
[[ ! nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 11 (bis) - OK" || echo "Test 11 (bis) - Erreur: sleep pas executé"
echo

# Test 12 : Mode 'or + cc' et les commandes n’échouent pas
echo "Test 12 : Mode 'or + cc' et les commandes n’échouent pas"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc "sleep 2" true && echo "Test 12 - OK" || echo "Test 12 - Erreur: and cc 'sleep 1' false a renvoyé true" 
[[ ! nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 12 (bis) - OK" || echo "Test 12 (bis) - Erreur: sleep pas executé"
echo

# Test 13 : Mode 'and + cc + kill' et une des commandes échoue plus vite que la terminaison des autres
echo "Test 13 : Mode 'and + cc + kill' et une des commandes échoue plus vite que la terminaison des autres"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc --kill "sleep 2" false && echo "Test 13 - Erreur: and cc 'sleep 1' false a renvoyé true" || echo "Test 13 - OK"
[[ nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 13 (bis) - OK" || echo "Test 13 (bis) - Erreur: sleep pas executé"
echo

# Test 14 : Mode 'and + cc + kill' et les commandes n’échouent pas
echo "Test 14 : Mode 'and + cc + kill' et les commandes n’échouent pas"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc --kill "sleep 2" true && echo "Test 14 - OK" || echo "Test 14 - Erreur: and cc 'sleep 1' false a renvoyé true"
[[ nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 14 (bis) - OK" || echo "Test 14 (bis) - Erreur: sleep pas executé"
echo

# Test 15 : Mode 'or + cc + kill' et une des commandes échoue plus vite que la terminaison des autres
echo "Test 15 : Mode 'or + cc + kill' et une des commandes échoue plus vite que la terminaison des autres"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc --kill "sleep 2" false && echo "Test 15 - Erreur: and cc 'sleep 1' false a renvoyé true" || echo "Test 15 - OK"
[[ nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 15 (bis) - OK" || echo "Test 15 (bis) - Erreur: sleep pas executé"
echo

# Test 16 : Mode 'or + cc + kill' et les commandes n’échouent pas
echo "Test 16 : Mode 'or + cc + kill' et les commandes n’échouent pas"
nbsleep=$(pgrep sleep | wc -l)
./do --and --cc --kill "sleep 2" true && echo "Test 16 - OK" || echo "Test 16 - Erreur: and cc 'sleep 1' false a renvoyé true"
[[ nbsleep -eq $(pgrep sleep | wc -l) ]] && echo "Test 16 (bis) - OK" || echo "Test 16 (bis) - Erreur: sleep pas executé"
echo
