TP PDS 6 - Do
=============

Ce dossier contient les fichiers du 6ème TP de PDS.

Contenu
-------

- README.md
  Ce fichier, expliquant comment fonctionne l'ensemble des programmes

- Makefile
  Fichier donnant les regles de compilation

- do.c
  Implementation de la commande do

- makeargv.{c,h}
  Bibliothèque fournie permettant de gerer plus facilement les arguments fournis

- tests.sh
  Script shell permettant de vérifier plusieurs cas d'utilisation

Executer
--------

Pour generer les fichiers binaires, executer :
```sh
$ make
```

Puis executer le script shell tests.sh pour vérifier les cas d'utilisation généré à l'aide des commandes true, false et sleep :
```sh
$ bash tests.sh
```

ou :
```sh
$ make tests
```

Auteur
------

Nicolas SEYS - L3 Groupe 3