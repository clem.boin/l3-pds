#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>

void usage(void) {
	printf("Usage:\n");
	printf(" maccess [options] <filename>\n");
	printf("\n");
	printf("Options:\n");
	printf(" -r\ttest if the file is readable\n");
	printf(" -w\ttest if the file is writable\n");
	printf(" -x\ttest if the file is executable\n");
	printf(" -v\tenable verbose mode\n");
	exit(EXIT_FAILURE);
}

void mperror(char *progname, char *filename) {
	fprintf(stderr, "%s: ", progname);
	
	switch(errno) {
	case EACCES:
		fprintf(stderr, "Permission denied\n");
		break;
	case ELOOP:
		fprintf(stderr, "Too many symbolic links\n");
		break;
	case ENAMETOOLONG:
		if (strlen(basename(filename)) > NAME_MAX)
			fprintf(stderr, "Filename is too long\n");
		else
			fprintf(stderr, "Pathname is too long\n");
		break;
	case ENOENT:
		fprintf(stderr, "File doesn't exists\n");
		break;
	case ENOTDIR:
		fprintf(stderr, "A component used as a directory in pathname is not a directory\n");
		break;
	}

	exit(errno);
}

int main(int argc, char **argv) {
	int boolr, boolw, boolx, boolv, res, opt;
	char *filename = NULL;
	
	boolr = boolw = boolx = boolv = 0;

	while ((opt = getopt(argc, argv, "rwxv")) != -1) {
		switch (opt) {
		case 'r':
			boolr = 1;
			break;
		case 'w':
			boolw = 1;
			break;
		case 'x':
			boolx = 1;
			break;
		case 'v':
			boolv = 1;
			break;
		case '?':
		default:
			usage();
		}
	}

	/* Check if there is at least 1 option (v excluded) */
	if (boolr + boolw + boolx < 1) {
		printf("%s: Missing option\n", basename(argv[0]));
		usage();
	}

	/* Get the filename and check if it exists*/
	filename = argv[optind];
	if (filename == NULL) {
		printf("%s: Missing argument\n", basename(argv[0]));
		usage();
	}

	/* Get the result value and do not check the next one if failed */
	if (boolr == 1)
		res = access(filename, R_OK);
	if (boolw == 1 && res == 0)
		res = access(filename, W_OK);
	if (boolx == 1 && res == 0)
		res = access(filename, X_OK);

	/* Return the error string on verbose mode */
	if (res != 0 && boolv == 1) {
		mperror(basename(argv[0]), filename);
	}
	
	return res;
}
