#!/bin/bash

mkdir tests

# Access Denied
echo -e '- Test1 : Access Denied'
touch tests/0
chmod a-rwx tests/0
./ft_maccess+ -rv tests/0

# Too many symbolic link
echo -e '\n- Test2 : Too many level of symbolic link'
cd tests
ln -s a b
ln -s b a
cd ..
./ft_maccess+ -rv tests/a

# Filename too long
echo -e '\n- Test3 : Filename too long'
filename=""
for x in {0..300}
do
	 filename=$filename'0'
done
./ft_maccess+ -rv $filename

# Path too long
echo -e '\n- Test4 : Path too long'
for x in {0..500}
do
	 pathname="$pathname/00000000000"
done
./ft_maccess+ -rv $pathname

# File not exists
echo -e '\n- Test5 : No such file or directory'
./ft_maccess+ -rv foo

# Not a directory
echo -e '\n- Test6 : Not a directory'
touch tests/1
./ft_maccess+ -r -v tests/1/a

# The end
rm -rf tests
