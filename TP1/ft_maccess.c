#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>

void usage(void) {
	printf("Usage:\n");
	printf(" maccess [options] <filename>\n");
	printf("\n");
	printf("Options:\n");
	printf(" -r\ttest if the file is readable\n");
	printf(" -w\ttest if the file is writable\n");
	printf(" -x\ttest if the file is executable\n");
	printf(" -v\tenable verbose mode\n");
	exit(-1);
}

void mperror(char *progname, char *filename) {
	fprintf(stderr, "%s: ", progname);
	
	switch(errno) {
	case EACCES:
		fprintf(stderr, "Permission denied\n");
		break;
	case ELOOP:
		fprintf(stderr, "Too many symbolic links\n");
		break;
	case ENAMETOOLONG:
		if (strlen(basename(filename)) > NAME_MAX)
			fprintf(stderr, "Filename is too long\n");
		else
			fprintf(stderr, "Pathname is too long\n");
		break;
	case ENOENT:
		fprintf(stderr, "File doesn't exists\n");
		break;
	case ENOTDIR:
		fprintf(stderr, "A component used as a directory in pathname is not, in fact, a director");
		break;
	}

	exit(errno);
}

int main(int argc, char **argv) {
	int boolr, boolw, boolx, boolv, res, i;
	char *filename = NULL;
	
	boolr = boolw = boolx = boolv = 0;

	if (argc != 3)
		usage();

	if (argv[1][0] == '-') {
		for (i = 1; i < strlen(argv[1]); i++) {
			switch (argv[1][i]) {
			case 'r':
				boolr = 1;
				break;
			case 'w':
				boolw = 1;
				break;
			case 'x':
				boolx = 1;
				break;
			case 'v':
				boolv = 1;
				break;				
			default:
				printf("Option inconnue '%s'\n", argv[1]);
				usage();
			}
		}
	} else {
		usage();
	}

	filename = argv[2];

	if (boolr == 1)
		res = access(filename, R_OK);
	else if (boolw == 1)
		res = access(filename, W_OK);
	else if (boolx == 1)
		res = access(filename, X_OK);

	if (boolr + boolw + boolx != 1) {
		printf("Trop d'options utilisées\n");
		usage();
	}
	
	if (res != 0 && boolv == 1) {
		mperror(basename(argv[0]), filename);
	}
	
	return res;
}
