TP PDS 1 - maccess
==================

Ce dossier contient les fichiers des exercices 1 à 4 du premier TP de PDS.

Contenu
-------

- Readme.md
  Ce fichier, expliquant comment fonctionne l'essemble des programmes

- Makefile
  Fichier donnant les regles de compilation

- ft_prlimit.c
  Affiche les variable NAME_MAX et PATH_MAX

- ft_maccess.c
  Permet de tester si un fichier à les droits de lecture, écriture et/ou execution.

- ft_maccess+.c
  Même fonctionnement que le precedent, en utilisant la fonction getopt (gestion des options plus efficace).

Executer
--------

Pour generer les fichiers binaires, executer :
```sh
$ make
```

Puis executer le fichier souhaité.
Pour afficher le mode d'utilisation de maccess, le lancer simplement sans options :
```sh
$ ./ft_maccess
```

Pour effectuer les tests, lancer :
```sh
$ make test
```

Ou :
```sh
$ ./tests.sh
```

Auteur
------

Nicolas SEYS - L3 Groupe 3